// 1. 创建vue根实例
// 2. 挂载App组件
require('./css/index.scss')

import Vue from 'vue'
import VueRouter from 'vue-router'

// 导入App.vue组件
import App from './App.vue'

const routes = require('./router/dynamic-router')

const router = new VueRouter({
    routes
})

Vue.use(VueRouter)
var vm = new Vue({
    components: { App },
    template: '<App/>',
    router
}).$mount('#app')
