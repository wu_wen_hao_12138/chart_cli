const baseConfig = require('./webpack.base')
const {merge} = require('webpack-merge')

const webpack = require('webpack')

const devConfig = {
    mode: "development", // development || production
    devServer: {
      contentBase: './dist', // 指定目录
      hot: true
    },
    // 插件
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
}

module.exports = merge(baseConfig, devConfig)