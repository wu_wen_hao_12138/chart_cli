const path = require('path')

// 引入vue-laoder插件， 在vue-loader@15版本开始需要在webpack引入该插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
    // 打包入口
    entry: './src/main.js',
    // 打包出口
    output: {
        publicPath: '',
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist')
    },
    // 打包规则
    module:{
        rules: [
            {
                test: /\.vue$/, 
                use: {
                    loader: 'vue-loader'
                }
            },
            {
                test: /\.(png|jpg|gif)$/i,
                use: [
                  {
                    loader: 'url-loader',
                    options: {  
                        name:'assets/[name].[ext]',
                        limit: 100 * 1024,
                    },
                  },
                ],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(html)$/,
                use: {
                  loader: 'html-loader',
                }
            }
        ]
    },
    // 插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new VueLoaderPlugin(),
        new CleanWebpackPlugin(),
    ],
    resolve: {
        alias: {
            'vue': 'vue/dist/vue.js'
        }
    }
}